## Overview

Let's start by stating the postulates of special relativity.

1. The laws of physics are the same in all inertial frames.
1. The speed of light is the same and invariant in all inertial frames.

Now consider the following experiment involving two observers:
1. one standing on the platform of a train station, and
1. one at the trailing edge of a single car express train.

When leading edge of the train car passes by the stationary observer, our moving observer throws the switch to a light source at the trailing edge. Both observers operate instruments to measure:

1. The duration required from the train cars leading and trailing edges to completely transit the stationary observer.
1. The length of the train car from each observer's respective frames of reference.

Do our observers' measurements agree or disagree?

### Time Dilation


Given:

1. $`c`$ : speed of light, the same and invariant in all intertial reference frames
1. $`v`$ : speed of moving train, agreed upon by both observers
1. $`d`$ : the width of a train car carrying our moving observer and his flashlight
1. $`\Delta{t}`$ : duration recorded by moving observer for light to traverse the width of the train car ($`d`$)
1. $`\Delta{\tilde{t}}`$ : duration recorded by observer on the platform for light to traverse width of the train car

Does $`\Delta{t} = \Delta{\tilde{t}}`$? If not, then derive an expression relating the two quantities.

#### Frame 1: The Train

```math
\Delta t = \frac{d}{c}
```

#### Frame 2: The Platform

```math
c^2 \Delta{\tilde{t}}^2 = d^2 + v^2 \Delta{\tilde{t}}^2
```

#### Deriving time dilation

```math
c^2 \Delta{\tilde{t}}^2 = d^2 + v^2 \Delta{\tilde{t}}^2
```

```math
\Delta{\tilde{t}}^2 = \frac{d^2}{c^2} + \frac{v^2}{c^2} \Delta{\tilde{t}}^2
```

```math
\Delta{\tilde{t}}^2 = \Delta{t}^2 + \frac{v^2}{c^2} \Delta{\tilde{t}}^2
```

```math
\Delta{\tilde{t}}^2 - \frac{v^2}{c^2} \Delta{\tilde{t}}^2 = \Delta{t}^2
```

```math
\left( 1 - \frac{v^2}{c^2} \right) \Delta{\tilde{t}}^2 = \Delta{t}^2
```

```math
\Delta{\tilde{t}} = \frac{\Delta{t}}{\sqrt{\left( 1 - \frac{v^2}{c^2} \right)}}
```


### Length contraction

Let's turn our attention to how time dilation impacts measurements of length. Assume both of our observers have instruments to measure the length of the station platform from their respective frames of reference.

Given:
1. $`L`$ : **Proper length** of the station platform measured by observer on station platform
1. $`\tilde{L}`$ : length of station of the platform measured by the moving observer

Does $`L = \tilde{L}`$? If not, then derive an expression relating the two quantities.

#### Frame 1: The Train

Recall that the **proper time** $`\Delta{t}`$ is measured by the moving observer while transiting the station.

```math
\tilde{L} = v \Delta{t}
```

#### Frame 2: The Platform

Also recall that the stationary observer measures the time taken for a train car to transit the station is $`\Delta{\tilde{t}}`$.

```math
L = v \Delta{\tilde{t}}
```

#### Prior result

We've already calculated proper time $`\Delta{t}`$ in the moving train's frame of reference to the duration measured by the observer on the platform. We will denote $`\frac{1}{\sqrt{\left( 1 - \frac{v^2}{c^2} \right)}}`$ as $`\gamma`$.

```math
\Delta{\tilde{t}} = \frac{\Delta{t}}{\sqrt{\left( 1 - \frac{v^2}{c^2} \right)}} = \gamma \Delta{t}
```

```math
\Delta{t} = \sqrt{\left( 1 - \frac{v^2}{c^2} \right)} \Delta{\tilde{t}} = \frac{\Delta{\tilde{t}}}{\gamma}
```

#### Deriving length contraction

```math
v = \frac{\tilde{L}}{\Delta{t}} = \frac{L}{\Delta{\tilde{t}}}
```

```math
\frac{\tilde{L}}{L} = \frac{\Delta{t}}{\Delta{\tilde{t}}}
```

```math
\frac{\tilde{L}}{L} = \frac{1}{\gamma}
```

```math
\tilde{L} = \frac{L}{\gamma} = \left( \sqrt{\left( 1 - \frac{v^2}{c^2} \right)} \right) L
```
