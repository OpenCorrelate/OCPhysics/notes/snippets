## Overview

A passenger aboard a relativistic rocket uses his watch to time the transit between Earth-Lunar Lagrange Points 4 and 5. Space stations at either point of libration observe the rocket's flyby at nearly head-on or sternward aspects respectively.

Neglecting accelerations due to thrust or gravity, and given:

1. $`c`$ : speed of light
1. $`v`$ : speed of the rocket
1. $`\Delta{t}`$ : the proper time interval of transit as measured by the rocket passenger
1. $`\Delta{\tilde{t}}`$ : the duration calculated by observers at L4 and L5
1. $`\Delta{x}`$ : the proper distance of transit as measured by observers at L4 and L5
1. $`\Delta{\tilde{x}}`$ : the distance calculated by the rocket passenger during transit

Will $`\Delta{t} = \Delta{\tilde{t}}`$? Will $`\Delta{x} = \Delta{\tilde{x}}`$? If not, derive expressions relating these quantities to one another.

### Preparation

First observe the postulates of special relativity:

1. The laws of physics are the same in all inertial frames.
1. The speed of light is the same and invariant in all inertial frames.

Now we attempt to determine the equation for time dilation.

### Time dilation

#### Frame 1 : The Rocket

#### Frame 2 : The stations

#### Deriving time Dilation
